import logo from './logo.svg';
import './App.css';
import {Customer} from './components/Customer.js'

function App() {
  return (
    <div className="App">
      <Customer></Customer>
    </div>
  );
}

export default App;
