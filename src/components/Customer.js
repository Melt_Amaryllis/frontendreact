//one of the final iterations
import React from 'react'
export class Customer extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            allcustomers : false,
            allinteractions : false, 
            individualinteractions : false,

            customer:[],
            interaction:[],
            isloaded : false,
            error: null,

            customer_id : 0,
            first_name : '',
            last_name : '',
            city : '',
            zipcode : 0,
            is_active : false,

            interaction_id : 0,
            interaction_customer_id : 0,
            description_of_interaction : '',
            method_of_interaction : '',
            issue_resolved : false,
        }
    }

    handleCustomerId = (e)=>{
        this.setState({customer_id:e.target.value})
    }

    handleFirstName = (e)=>{
        this.setState({first_name:e.target.value})
    }

    handleLastName = (e)=>{
        this.setState({last_name:e.target.value})
    }

    handleCity = (e)=>{
        this.setState({city:e.target.value})
    }
    
    handleZipcode = (e)=>{
        this.setState({zipcode:e.target.value})
    }

    handleIsActive = (e)=>{
        this.setState({is_active:e.target.checked})
    }

    handleInteractionId = (e)=>{
        this.setState({interaction_id:e.target.value})
    }

    handleInteractionCustomerId = (e)=>{
        this.setState({interaction_customer_id:e.target.value})
    }

    handleDescriptionOfInteraction = (e)=>{
        this.setState({description_of_interaction:e.target.value})
    }

    handleMethodOfInteraction = (e)=>{
        this.setState({method_of_interaction:e.target.value})
    }

    handleIssueResolved = (e)=>{
        this.setState({issue_resolved:e.target.checked})
    }

    updateCustomers(customer_id, first_name, last_name, city, zipcode, is_active){
        const customerToUpdate = {
            "first_name" : first_name,
            "last_name" : last_name,
            "city" : city,
            "zipcode" : zipcode,
            "date_registered" : new Date(),
            "is_active" : is_active
        }
        console.log(customerToUpdate)

        let userURL = 'http://localhost:8080/api/customer/update/' + customer_id
        fetch(userURL, {
            method: 'PUT', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(customerToUpdate),
            }) 
    }

    // interaction_id : 0,
    //         interaction_customer_id : 0,
    //         description_of_interaction : '',
    //         method_of_interaction : '',
    //         issue_resolved : false,

    updateInteraction(interaction_id, interaction_customer_id, description_of_interaction, method_of_interaction, issue_resolved){
        const interactionToUpdate = {
            "id" : interaction_id,
            "individual_customer_id" : interaction_customer_id,
            "date_of_interaction" : new Date(),
            "description_of_interaction" : description_of_interaction,
            "method_of_interaction" : method_of_interaction,
            "issue_resolved" : issue_resolved
        }
        console.log(interactionToUpdate)

        let userURL = 'http://localhost:8080/api/interaction/update/' + interaction_id
        fetch(userURL, {
            method: 'PUT', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(interactionToUpdate),
            }) 
    }

    addInteraction(interaction_customer_id, description_of_interaction, method_of_interaction, issue_resolved){
        const interactionToAdd = {
            "individual_customer_id" : interaction_customer_id,
            "date_of_interaction" : new Date(),
            "description_of_interaction" : description_of_interaction,
            "method_of_interaction" : method_of_interaction,
            "issue_resolved" : issue_resolved
        }
        console.log(interactionToAdd)

        let userURL = 'http://localhost:8080/api/interaction/add'
        fetch(userURL, {
            method: 'POST', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(interactionToAdd),
            }) 
            .then()
            .then()
    }

    putAPI(first_name, last_name, city, zipcode, is_active){
        const customerToAdd = {
            "first_name" : first_name,
            "last_name" : last_name,
            "city" : city,
            "zipcode" : zipcode,
            "date_registered" : new Date(),
            "is_active" : is_active
        }
        console.log(customerToAdd)

        let userURL = 'http://localhost:8080/api/customer/add'
        fetch(userURL, {
            method: 'POST', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(customerToAdd),
            }) 
            .then()
            .then()
    }
    
    getAllInteractions(){
        let userURL = 'http://localhost:8080/api/interaction/all'
        fetch(userURL)
            .then(res => res.json())
            .then(
                (result) =>{
                    console.log(result)
                    this.setState({isloaded : true, interaction : result, allinteractions : true})
                },
                (error)=>{
                    this.setState({isloaded : true, error : error})
                }
            )
    }

    callAPI(){
        let userURL = 'http://localhost:8080/api/'
        fetch(userURL)
            .then(res => res.json())
            .then(
                (result) =>{
                    console.log(result)
                    this.setState({isloaded : true, customer : result, allcustomers : true})
                },
                (error)=>{
                    this.setState({isloaded : true, error : error})
                }
            )
    }

    getinteractionlist(customer_id){
        console.log(customer_id)
        let userURL = 'http://localhost:8080/api/customer/interactions/' + customer_id
        fetch(userURL)
            .then(res => res.json())
            .then(
                (result) =>{
                    console.log(result)
                    this.setState({isloaded: true, interaction : result, individualinteractions : true})
                },
                (error)=>{
                    this.setState({isloaded : true, error : error})
                }
            )
    }

    componentDidMount() {
        // this.callAPI()
    }

    render() {
        if (this.state.error) {
            return(
                <aside>
                    <h4>ERROR : {this.state.error.message}</h4>
                </aside>
            )
        } else if (!this.state.isloaded) {
            return(
                <aside>
                    <h2> Customer Menu </h2>
                    <section>
                        <ul>   
                            Customer ID: <input customer_id = "customer_id" onChange = {this.handleCustomerId}></input> <br></br>
                            First Name: <input first_name = "first_name" onChange = {this.handleFirstName}></input> <br></br>
                            Last Name: <input last_name = "last_name" onChange = {this.handleLastName}></input> <br></br>
                            City Name: <input city = "city>" onChange = {this.handleCity}></input> <br></br>
                            Zipcode: <input zipcode = "zipcode" onChange = {this.handleZipcode}></input> <br></br>
                            Is Active: <input type = "checkbox" onChange = {this.handleIsActive}></input> <br></br>
                            <button onClick = { () => {this.callAPI()}}>Get All</button> <br></br>
                            <button onClick = { () => {this.putAPI(this.state.first_name, this.state.last_name, this.state.city, this.state.zipcode, this.state.is_active)}}>Add Customer</button> <br></br>
                            <button onClick = { () => {this.updateCustomers(this.state.customer_id, this.state.first_name, this.state.last_name, this.state.city, this.state.zipcode, this.state.is_active)}}>Update</button> <br></br>
                        </ul>
                    </section>

                    <h2> Interactions Menu </h2>
                    <section>
                        <ul>
                            Interaction ID: <input interaction_id = "interaction_id" onChange = {this.handleInteractionId}></input> <br></br>
                            Customer ID: <input interaction_customer_id = "interaction_customer_id" onChange = {this.handleInteractionCustomerId}></input> <br></br>
                            Description of Interaction: <input description_of_interaction = "description_of_interaction>" onChange = {this.handleDescriptionOfInteraction}></input> <br></br>
                            Method of Interaction: <input method_of_interaction = "method_of_interaction" onChange = {this.handleMethodOfInteraction}></input> <br></br>
                            Issue Resolved: <input type = "checkbox" onChange = {this.handleIssueResolved}></input> <br></br>
                            <button onClick = { () => {this.getAllInteractions()}}>Get All</button> <br></br>
                            {/* interaction_id : 0,
                                interaction_customer_id : 0,
                                description_of_interaction : '',
                                method_of_interaction : '',
                                issue_resolved : false, */}
                            <button onClick = { () => {this.addInteraction(this.state.interaction_customer_id, this.state.description_of_interaction, this.state.method_of_interaction, this.state.issue_resolved)}}>Add Interaction</button> <br></br>
                            <button onClick = { () => {this.updateInteraction(this.state.interaction_id, this.state.interaction_customer_id, this.state.description_of_interaction, this.state.method_of_interaction, this.state.issue_resolved)}}>Update</button> <br></br>
                        </ul>
                    </section>

                </aside>
            )
        } else if (this.state.individualinteractions) {
            return (
                <section>
                    <ul>
                        <button onClick = {() => this.setState( {individualinteractions:false} )}>return</button>
                        {this.state.interaction.map( interaction => (
                            <li>
                                <h3>
                                    Interaction ID: {interaction.id}<br></br>
                                </h3>
                                <h4>
                                    Customer ID: {interaction.individual_customer_id}<br></br>
                                    Method of Interaction: {interaction.method_of_interaction}<br></br>
                                    Description of Interaction: {interaction.description_of_interaction}<br></br>
                                    Date of Interaction: {interaction.date_of_interaction}<br></br>
                                    Is Resolved: {interaction.issue_resolved.toString()}<br></br>
                                </h4>
                            </li>
                        ))}
                    </ul>
                </section>
            )
        } else if (this.state.allcustomers){
            return(
                <section>
                    <ul>
                        <button onClick = {() => this.setState( {isloaded:false, allcustomers:false} )}>return</button>
                        {this.state.customer.map( customer => (
                            <li>
                                <h3>Customer ID: {customer.id}</h3>
                                {/* <button onClick = {() => this.setState ( {individualinteractions:true})}> */}
                                <button onClick = { () => {this.getinteractionlist(customer.id)}}>
                                <h4>
                                    Full Name: {customer.first_name} {customer.last_name} <br></br>
                                    City: {customer.city}<br></br>
                                    Zip: {customer.zipcode}<br></br>
                                    Date Registered: {customer.date_registered} <br></br>
                                    Is Active: {customer.is_active.toString()} <br></br>
                                    {/* Interactions: 
                                    {
                                        <ul>
                                            {customer.interactionsList.map (interaction => (
                                                <h5>
                                                    Interaction ID: {interaction.id}<br></br>
                                                    Method of Interaction: {interaction.method_of_interaction}<br></br>
                                                    Description of Interaction: {interaction.description_of_interaction}<br></br>
                                                    Date of Interaction: {interaction.date_of_interaction}<br></br>
                                                    Is Resolved: {interaction.issue_resolved.toString()}<br></br>
                                                </h5>
                                            ))}
                                        </ul>
                                    } */}
                                </h4>
                                </button>
                            </li>
                        ) )}
                    </ul>
                </section>
            )
        } else if (this.state.allinteractions) {
            return(
                <section>
                    <ul>
                        <button onClick = {() => this.setState( {isloaded:false, allinteractions:false} )}>return</button>
                        {this.state.interaction.map( interaction => (
                            <li>
                                <h3>
                                    Interaction ID: {interaction.id}<br></br>
                                </h3>
                                <h4>
                                    Customer ID: {interaction.individual_customer_id}<br></br>
                                    Method of Interaction: {interaction.method_of_interaction}<br></br>
                                    Description of Interaction: {interaction.description_of_interaction}<br></br>
                                    Date of Interaction: {interaction.date_of_interaction}<br></br>
                                    Is Resolved: {interaction.issue_resolved.toString()}<br></br>
                                </h4>
                            </li>
                        ))}
                    </ul>
                </section>
            )
        } 
    }
}